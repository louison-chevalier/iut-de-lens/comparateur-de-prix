<!doctype html>
<html lang="fr" xmlns:table="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simulateur prix</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4 text-center">Simulateur de prix</h1>
        </div>
        <?php
            //Valeur par défault (on n'affiche pas la section affiche)
            $sectionaffiche = 0;
            
            // Si l'utilisateur envoie le formulaire
            if(isset($_POST['btAjouter'])){
                $prix = $_POST['prix'];
                $nbartcile = $_POST['nbarticle'];
                $somme = $prix*$nbartcile;
                $sectionaffiche = 1;

                //Paypal
                if($somme <2500){
                    $paypal = ($somme*3.4)/100+($nbartcile*0.25);
                }
                elseif ($somme < 10000){
                    $paypal = ($somme*2)/100+($nbartcile*0.25);
                }
                else{
                    $paypal = ($somme*1.8)/100+($nbartcile*0.25);
                }
                $pourcpaypal = ($paypal/$somme)*100;

                //Payplug
                $payplug1 = ($somme*2.5)/100+($nbartcile*0.25)+0;
                $payplug2 = ($somme*0.8)/100+($nbartcile*0.15)+30;
                $payplug3 = ($somme*0.5)/100+($nbartcile*0.15)+80;
                $pourcpayplug1 = ($payplug1/$somme)*100;
                $pourcpayplug2 = ($payplug2/$somme)*100;
                $pourcpayplug3 = ($payplug3/$somme)*100;


                //Stripe
                $stripe = ($somme*1.4)/100+($nbartcile*0.25);
                $pourcstripe = ($stripe/$somme)*100;
            }
        ?>
        <form  method="post"  action="index.php">
            <input required type="number" id="nbarticle"  placeholder="Nombre d'article" name="nbarticle" min="0">
            <input required type="number" placeholder="Prix" id="prix" name="prix" min="0">
            <input  type="submit" id="btAjouter" name="btAjouter" value="Tester">
        </form>
        <br>
        <?php if ($sectionaffiche == 1){?>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Service</th>
                <th scope="col">Commission sur <?php echo $nbartcile;?> article à <?php echo $prix;?>€ soit <?php echo $somme;?>€</th>
                <th>Commission en %</th>
                <th>Votre revenu sur <?php echo $somme;?>€</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Paypal</th>
                <td> <?php echo $paypal;?>€</td>
                <td> <?php echo $pourcpaypal;?>%</td>
                <td> <?php echo $somme-$paypal;?>€</td>
            </tr>
            <tr>
                <th scope="row">Payplug</th>
                <td>Offre Starter <?php echo $payplug1;?>€ <br> Offre Pro <?php echo $payplug2;?>€ <br> Offre Premium <?php echo $payplug3;?>€</td>
                <td>Offre Starter <?php echo $pourcpayplug1;?>% <br> Offre Pro <?php echo $pourcpayplug2;?>% <br> Offre Premium <?php echo $pourcpayplug3;?>%</td>
                <td>Offre Starter <?php echo $somme-$payplug1;?>€ <br> Offre Pro <?php echo $somme-$payplug2;?>€ <br> Offre Premium <?php echo $somme-$payplug3;?>€</td>

            </tr>
            <tr>
                <th scope="row">Stripe</th>
                <td><?php echo $stripe;?>€</td>
                <td><?php echo $pourcstripe;?>%</td>
                <td> <?php echo $somme-$stripe;?>€</td>
            </tr>
            </tbody>
        </table>
        <?php } ?>
    </div>
</body>
</html>
